# Create training data, models

```
mkdir saved-models
python3 runme.py SVD_BANK.xml.gz O4-H.xml
```

This will output h5 files that store model weights in the saved-models
directory. It will also produce an overlaps.txt file that contains the physical
waveform overlaps with the basis vectors.

# Check model

```
python3 eval-model.py SVD_BANK.xml.gz O4-H.xml overlaps.txt
```

This will randomly sample within the convex hull of the space and create a histogram of the mismatches.
