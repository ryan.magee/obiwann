#!/usr/bin/env python3

import sys
import tensorflow as tf
print(tf.executing_eagerly())
from obiwann import convex
import numpy
from gstlal import cbc_template_fir
from gstlal import svd_bank
from obiwann.utils import read_psd
from ligo.lw import ligolw
from ligo.lw import utils as ligolw_utils
from ligo.lw import lsctables
lsctables.use_in(svd_bank.DefaultContentHandler)
import matplotlib
from obiwann import utils
from obiwann import model

filename = sys.argv[1]
bank, template_bank, time_slices = utils.parse_svd_bank(filename)

# Load PSD, templates from SVD to define region of validity
psd_file = sys.argv[2]
psd = read_psd(psd_file, verbose=False)['H1']

# Create sample points, condition templates, project onto bases
# to provide training data for the net
samples, _ = utils.sample_svdhull(filename, num_samples = 100000)
templates, sngl_tbl = utils.condition_templates(samples, psd, time_slices)
data = utils.project(bank, templates, sngl_tbl)
data = model.TrainingData(bank_fragment = bank, original_data = data)
data.set_normalized_data(stopping_ind = None)
data.save_data('L1-test-overlaps.txt')


# Everything above here can be run as a pre-processing step to avoid
# waiting while generating a large number of training samples
#data = model.TrainingData(bank_fragment=bank, filename = 'overlaps.txt')
#data.set_normalized_data(stopping_ind = None)

# Iterate over bases to create our complete model
net = model.Model(data.normalized_inputs)

net.fit_model(data.normalized_inputs[:,:2], data.normalized_outputs, save_model = True, output_dir = 'L1-test-saved-models', output_name = 999)
