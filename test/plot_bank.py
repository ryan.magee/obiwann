#!/usr/bin/env python3

import sys
import numpy
from gstlal import cbc_template_fir
from gstlal import svd_bank
from gstlal.psd import read_psd
from psd import read_psd
from ligo.lw import ligolw
from ligo.lw import utils as ligolw_utils
from ligo.lw import lsctables
lsctables.use_in(svd_bank.DefaultContentHandler)
import matplotlib
matplotlib.use('agg')

from matplotlib import pyplot as plot
import model

_sit_cols = lsctables.SnglInspiralTable.validcolumns
class SnglInspiralTable(lsctables.SnglInspiralTable):
	def __init__(self, *args, **kwargs):
		lsctables.SnglInspiralTable.__init__(self, *args, **kwargs)
		for entry in _sit_cols.keys():
			if not(hasattr(self, entry)):
				if _sit_cols[entry] in ['real_4', 'real_8']:
					setattr(self, entry, 0.)
				elif _sit_cols[entry] == 'int_4s':
					setattr(self, entry, 0)
				elif _sit_cols[entry] == 'int_8s':
					setattr(self, entry, 0)
				elif _sit_cols[entry] == 'lstring':
					setattr(self, entry, '')
				elif _sit_cols[entry] == 'ilwd:char':
					setattr(self, entry, '')
				else:
					#print("Column %s not recognized" % entry, file=sys.stderr)
					raise ValueError

filename = sys.argv[1]
banks = svd_bank.read_banks(filename, contenthandler = svd_bank.DefaultContentHandler)

# For now, isolate the last bank fragment
for bank in banks:
	for frag in bank.bank_fragments:
		if frag.start == 0.0:
			time_slices = numpy.array([(frag.rate, frag.start, frag.end),],dtype=[('rate','int'),('begin','float'),('end','float')])
			my_bank = frag

temps = banks[0].sngl_inspiral_table
points = []
for temp in temps:
	m1 = max(temp.mass1, temp.mass2)
	m2 = min(temp.mass1, temp.mass2)
	points.append((m1, m2))

points = numpy.array(points)
if False:
	plot.scatter(points[:,0],points[:,1], color = 'k', alpha = 1)
else:
	data = model.TrainingData(bank_fragment=my_bank, filename = 'test.txt')
	plot.scatter(data.original_data[2][:,1], data.original_data[2][:,2], c=numpy.real((data.original_data[2][:,-2] + 1.j*data.original_data[2][:,-1])*numpy.exp(-1.j*numpy.arctan2(data.original_data[0][:,-1], data.original_data[2][:,-2]))))
	plot.colorbar()
	print(data.original_data[2][:,0])
plot.xlim([30,75])
plot.ylim([0,40])
plot.savefig('bank.png')
