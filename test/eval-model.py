#!/usr/bin/env python

import sys
from obiwann import utils
import numpy
from obiwann import model
from obiwann.utils import read_psd
from ligo.lw import lsctables
from obiwann import convex

import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plot


# Load SVD bank, PSD
filename = sys.argv[1]
bank, template_bank, time_slices = utils.parse_svd_bank(filename)
psd_file = sys.argv[2]
psd = read_psd(psd_file, verbose=False)['H1'] 

# Load training data and ML models
data = model.TrainingData(bank_fragment = bank, filename = sys.argv[3])
data.set_normalized_data(stopping_ind = None)
net = model.Model(data.normalized_inputs)

# Sample inside the hull of the SVD to get testing points
samples, points = utils.sample_svdhull(filename, num_samples = 1000)

weights = {}
for basis in range(data.num_bases):
	net.load_model(basis)

	# Could separate to another loop for clarity.
	# Evaluate predictions for all sample points
	pmean, pstd = data.input_scaling[basis]
	mean, std = data.output_scaling[basis]
	test_input = numpy.array(points)
	test_input = (test_input - pmean) / pstd
	pred = net.models[basis].predict(test_input)
	pred = pred*std+mean
	weights[basis] = numpy.array(pred)

# Force imaginary components of first basis projections to be identically 0
weights[0][:,1] = 0

# Make array of time series arrays
new = numpy.array([numpy.zeros(len(bank.orthogonal_template_bank[0]), dtype = 'cfloat') for temp in range(len(points))])
ones = numpy.ones(len(points))
for i, basis in enumerate(bank.orthogonal_template_bank):
	basis = numpy.outer(ones, basis)
	new += weights[i][:,:1] * basis + 1.j*weights[i][:,1:] * basis

template_bank, _ = utils.condition_templates(samples, psd, time_slices)
points = numpy.array(points)

temp = template_bank[0]
matches = []
for i in range(len(points)):
	ref = (temp[2*i,:] + 1.j*temp[2*i+1,:])
	offset = numpy.argmax(numpy.abs(ref))-numpy.argmax(numpy.abs(new[i,:]))
	match = utils.match(ref, new[i,:])
	matches.append(match)

bins = numpy.logspace(-4,0,1000)
plot.hist(1-numpy.array(matches), bins=bins, density = True, cumulative = -1)
plot.xlabel('Mismatch')
plot.ylabel('CDF')
plot.xlim([1e-4,1])
plot.xscale('log')
plot.savefig('test.png')
