import setuptools

with open("README.md", "r") as fh:
	long_description = fh.read()

setuptools.setup(
	name = "OBIWANN",
	version = "0.0.1",
	author = "Ryan Magee",
	author_email = "ryan.magee@ligo.org",
	description = "Orthogonal Basis Informed Waveform Approximation with Neural Nets",
	long_description = long_description,
	long_description_content_type = "text/markdown",
	url = "https://git.ligo.org/ryan.magee/obiwann",
	packages = ['obiwann'],
	zip_safe = False,
	classifiers = [
		"Programming Language :: Python :: 3",
		"License :: OSI Approved :: GPLv2",
		"Operating System :: OS Independent",
	],
	python_requires = '>=3.7',
)

