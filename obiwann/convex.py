#!/usr/bin/env python3

import numpy
from scipy.optimize import linprog
from gstlal import svd_bank
from obiwann import utils

class hull(object):
	def __init__(self, points):
		self.points = numpy.array(points)
	# A clever way to check for inclusion in a hull,
	# taken from https://stackoverflow.com/a/43564754
	# Checks if x is the in convex hull defined by points
	def in_hull(self, x):
		n_points = len(self.points)
		n_dim = len(x)
		c = numpy.zeros(n_points)
		A = numpy.r_[self.points.T,numpy.ones((1,n_points))]
		b = numpy.r_[x, numpy.ones(1)]
		# Search for solution Ay=b, e.g. can b be formed from a linear combination of A
		lp = linprog(c, A_eq=A, b_eq=b)
		return lp.success

class svdhull(object):
	def __init__(self, filename):
		frag, temps, _ = utils.parse_svd_bank(filename)
		self.mmin = numpy.inf
		self.mmax = 0.
		self.qmin = numpy.inf
		self.qmax = 0.
		points = []
		for temp in temps:
			m1 = max(temp.mass1, temp.mass2)
			m2 = min(temp.mass1, temp.mass2)
			points.append((m1, m2))
			self.mmin = min(self.mmin, m1+m2)
			self.mmax = max(self.mmax, m1+m2)
			self.qmin = min(self.qmin, m1/m2)
			self.qmax = max(self.qmax, m1/m2)
		self.points = numpy.array(points)
		self.hull = hull(points)
	def in_hull(self, x):
		return self.hull.in_hull(x)


