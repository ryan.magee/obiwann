#!/usr/bin/env python3

import lal
from obiwann import convex
import numpy
from gstlal import cbc_template_fir
from gstlal import svd_bank
from ligo.lw import ligolw
from ligo.lw import utils as ligolw_utils
from ligo.lw import lsctables
lsctables.use_in(svd_bank.DefaultContentHandler)

#####################
# Helper functions
#####################

def mchirp(m1, m2):
	return (m1*m2)**(3./5) / (m1 + m2)**(1./5)

def q(m1, m2):
	return min(m1, m2) / max(m1, m2)

def eta(m1, m2):
	return m1*m2/(m1+m2)**2.

def M(m1, m2):
	return m1+m2

def m1_m2_of_M_q(M, q):
	return M / (1+q), M *(1 - 1. / (1+q))

# Helper class stolen from sbank that initializes empty columns in SnglInspiralTables
_sit_cols = lsctables.SnglInspiralTable.validcolumns
class SnglInspiralTable(lsctables.SnglInspiralTable):
	def __init__(self, *args, **kwargs):
		lsctables.SnglInspiralTable.__init__(self, *args, **kwargs)
		for entry in _sit_cols.keys():
			if not(hasattr(self, entry)):
				if _sit_cols[entry] in ['real_4', 'real_8']:
					setattr(self, entry, 0.)
				elif _sit_cols[entry] == 'int_4s':
					setattr(self, entry, 0)
				elif _sit_cols[entry] == 'int_8s':
					setattr(self, entry, 0)
				elif _sit_cols[entry] == 'lstring':
					setattr(self, entry, '')
				elif _sit_cols[entry] == 'ilwd:char':
					setattr(self, entry, '')
				else:
					raise ValueError

def read_psd(filename, verbose = False):
	return lal.series.read_psd_xmldoc(ligolw_utils.load_filename(filename, verbose = verbose, contenthandler = lal.series.PSDContentHandler))

def parse_svd_bank(filename):
	banks = svd_bank.read_banks(filename, contenthandler = svd_bank.DefaultContentHandler)
	# For now, isolate the last bank fragment
	for bank in banks:
		for frag in bank.bank_fragments:
			if frag.start == 0.0:
				time_slices = numpy.array([(frag.rate, frag.start, frag.end),],dtype=[('rate','int'),('begin','float'),('end','float')])
				my_frag = frag
	sngl_inspiral_table = banks[0].sngl_inspiral_table

	return my_frag, sngl_inspiral_table, time_slices


def sample_svdhull(filename, num_samples = 10, seed = 10):
	numpy.random.seed(seed)
	tbl = lsctables.New(lsctables.SnglInspiralTable)
	
	# Construct rectangular bounded template bank and randomly sample points inside of it
	# FIXME generalize, this presently samples a rectangle in (mt, q) space
	n = 0
	hull = convex.svdhull(filename)
	m1lower = min(hull.points[:,0])
	m1upper = max(hull.points[:,0])
	m2lower = min(hull.points[:,1])
	m2upper = max(hull.points[:,1])
	points = []
	mts = numpy.random.uniform(hull.mmin, hull.mmax, num_samples)
	qs = numpy.random.uniform(hull.qmin, hull.qmax, num_samples)
	m1s, m2s = m1_m2_of_M_q(mts, qs)
	for i in range(num_samples):
		row = SnglInspiralTable()
		row.mass1, row.mass2 = m1s[i], m2s[i]
		row.mass1, row.mass2 = m1s[i], m2s[i]
		tbl.append(row)
		phase = 2*numpy.random.random()*numpy.pi
		points.append((row.mass1, row.mass2,0,1))
	return tbl, points


def condition_templates(sngl_inspiral_table, psd, time_slices, approx = 'IMRPhenomC'):
	# Generate whitened templates for all random samples.
	# We hard code this to only consider last time slice
	template_bank, _, _, _, bank_workspace = cbc_template_fir.generate_templates(
				sngl_inspiral_table,
				approx,
				psd,
				10.,
				time_slices = time_slices,
				autocorrelation_length = 351,
				fhigh = 1024.,
				verbose = False)
	return template_bank, sngl_inspiral_table

def project(frag, physical_bank, temp_tbl):
	# iterating over single timeslice for now
	data = []
	for time_slice in physical_bank:
		# time_slice dimensions are 2*numtemps, time samples
		for j in range(int(len(time_slice)/2)):
			out = []
			tmp = time_slice[2*j,:] + 1.j * time_slice[2*j+1,:]
			# Get time and phase at max strain
			ind = numpy.argmax(abs(tmp))
			phase = numpy.arctan2(tmp[ind].imag, tmp[ind].real)
			rot = numpy.exp(-1.j*phase)
			tmp *= rot
			for i, orthogonal_template in enumerate(frag.orthogonal_template_bank):
				# Calculate the overlap between the orthogonal template and both polarizations.
				# Store array as [svdnum, mass1, mass2, Re(overlap), Im(overlap)]
				overlap = numpy.dot(orthogonal_template, tmp)
				if i == 0:
					out.extend([temp_tbl[j].mass1, temp_tbl[j].mass2])
				out.extend([overlap.real, overlap.imag])
			data.append(tuple(out))
	data = numpy.array(data)
	return data

def match(waveform1, waveform2):
	# Takes in two complex time series of arbitrary normalization and maximizes
	# over time and phase to produce the match
	# FIXME to roll or not to roll? 
	offset = numpy.argmax(numpy.abs(waveform1))-numpy.argmax(numpy.abs(waveform2))
	return numpy.abs(numpy.dot(waveform1, numpy.conj(waveform2))) / ( numpy.abs(numpy.dot(waveform1, numpy.conj(waveform1)))**.5 *  numpy.abs(numpy.dot(waveform2, numpy.conj(waveform2)))**.5 )


