#!/usr/bin/env python3

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras.callbacks import EarlyStopping
import numpy
from obiwann import convex
from gstlal import svd_bank

##################
# Data loader
##################

class TrainingData(object):
	def __init__(self, bank_fragment, filename = None, original_data = None):
		self.filename = filename
		self.frag = bank_fragment
		self.num_bases = len(self.frag.orthogonal_template_bank)

		# This takes a second, so just set this attribute while loading
		if filename:
			self.original_data = self.load_data()
		elif original_data is not None:
			self.original_data = original_data
			self.frag = bank_fragment
		else:
			raise ValueError('Must supply data via file or directly')

	def load_data(self):
		data = numpy.loadtxt(self.filename)
		return data

	def save_data(self, output_file):
		# FIXME requires knowledge of number of columns
		data = self.original_data
		numpy.savetxt(output_file, data)
	
	def rescale(self, input_data):
		# Rescale the input data such that it has mean 0 and variance 1
		# Return mean, std to allow for inverse transformation/scaling
		# new model inputs
		mean = numpy.mean(input_data.T)
		std = numpy.std(input_data.T)
		scaled_data = (input_data - mean) / std
		return scaled_data, mean, std
	
	def normalize_data(self, data_by_bin, binnum = 0, stopping_ind = 1000):
		binnum = int(binnum)
		data = data_by_bin#[binnum]
		params = []
		matches = []
		# Rotate the overlap for each template such that the overlap is purely real
		# for the first basis vector. This attempts to mitigate some of the high 
		# variability in overlaps that is empirically observed
		# FIXME unused right now
		# phase = {i: numpy.exp(-1.j * numpy.arctan2(temp[3], temp[2])) for i, temp in enumerate(data_by_bin)}
		for i, temp in enumerate(data[:stopping_ind]):
			m1, m2, *overlaps = temp
			overlaps = numpy.array(overlaps)
			params.append((m1,m2))
			matches.append(overlaps)

		params = numpy.array(params)
		matches = numpy.array(matches)
		matches = matches.reshape((len(matches),-1))

		# Rescale inputs and targets. We assumed overlaps should range -1 to 1, instead, we scale to 0 to 1
		matches, mean, std = self.rescale(matches)
		params, pmean, pstd = self.rescale(params)
		
		return params, matches, (mean, std), (pmean, pstd)

	def set_normalized_data(self, stopping_ind = 1000):
		self.normalized_inputs = {}
		self.normalized_outputs = {}
		self.input_scaling = {}
		self.output_scaling = {}
		self.normalized_inputs, self.normalized_outputs, self.output_scaling, self.input_scaling = self.normalize_data(self.original_data, stopping_ind = stopping_ind)


#####################
# Construct model
#####################

class Model(object):
	def __init__(self, input_data):
		self.input_data = input_data
		self.models = {}

	def model(self, input_data, output_data):
		input_size = input_data.shape[-1]
		output_size = output_data.shape[-1]
		inputs = tf.keras.Input(shape=(input_size,))
		norm = layers.Normalization(axis=-1)
		norm.adapt(numpy.array(input_data))
		norm = norm(inputs)
		x = layers.Dense(1000, activation='relu')(norm)
		x = layers.Dense(1000, activation='relu')(x)
		x = layers.Dense(1000, activation='relu')(x)
		x = layers.Dense(output_size, activation='linear')(x)
	
		models = tf.keras.Model(inputs,x)
		return models

	def fit_model(self, input_data, output_data, save_model = False, output_dir = 'saved-models', output_name = 0):
		models = self.model(input_data, output_data)
		callback = EarlyStopping(monitor='val_loss', min_delta=0, patience=10)
		custom_optimizer=tf.keras.optimizers.SGD(learning_rate=0.1, momentum=0.9)
		models.compile(optimizer=custom_optimizer,loss='mean_squared_error')
		models.fit(input_data, output_data, batch_size=1000,epochs=10000,verbose=1, validation_split=0.2, shuffle=False, callbacks=[callback])
		if save_model:
			numpy.random.seed(0)
			tf.random.set_seed(0)
			models.save('%s/%d.h5' % (output_dir, output_name))
		self.models[output_name] = models
		return models

	def load_model(self, name, model_dir = 'saved-models'):
		model = tf.keras.models.load_model('%s/%d.h5' % (model_dir, name))
		self.models[name] = model


