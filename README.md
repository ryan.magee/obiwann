# obiwann

Orthogonal Basis Informed Waveform Approximation with Neural Nets

## Getting started

### Installation

Requires standard IGWN software + tensorflow + GstLAL. The yml file included in the base directory is sufficient. 

```conda env create -f env.yml -n <env>```

After dependencies are installed, activate the environment and install Obiwann directly with pip:

```pip install .```

Note that to use GPUs, you may need to export the appropriate flags. This can be done each time:

```export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/```

or you can add to your conda env's activation script.

### Test scripts

A minimum working example is included in ```test/```. After entering that directory, run:

```python3 runme.py SVD_BANK.xml.gz O4-H.xml```

to create and save a model for a non-spinning BBH SVD. To evaluate the model's performance, run:

```python3 eval_model.py SVD_BANK.xml.gz O4-H.xml overlaps.txt```

This will produce a histogram of interpolation mismatches. The test example only uses 1000 training points per basis, but this is simple to change. Note that at the moment, some features are hardcoded:
1) There is no spin
2) Coalescence phase is arbitrary
3) The model specifics are hardcoded in ```model.py```

The model works equally well for 1+2, and generalization is underway. The model choice in 3 was empirically found to be the best, but feel free to tweak and test locally.

### Using GstLAL's composite detection statistic

A MWE to show how to rapidly produce arbitrary time series from an injection is in progress.
